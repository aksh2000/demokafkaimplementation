package com.aksh.apachekafkaconsumer.consumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

    public static final String TOPIC = "test";
    public static final String GROUP_ID = "group_id";

    @KafkaListener(topics = TOPIC, groupId = GROUP_ID)
    public void consumer(String message){
        System.out.println("Message received from producer : " + message);
    }
}
