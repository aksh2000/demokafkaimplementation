# demoKafkaImplementation

Kafka Consumer and Producer Mock Implementation using SpringBoot

## Instructions

Open Terminal and run the following commands. (To install Apache Kafka `brew install kafka`)

1. `zkServer start`
2. `kafka-server-start /usr/local/etc/kafka/server.properties`
3. `kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test`
4. `kafka-console-consumer --bootstrap-server localhost:9092 --topic test --from-beginning` - To View the Consumer Console

